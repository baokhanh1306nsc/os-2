#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/random.h>

#define DEVICE_NAME "RandomGenerator"

static int dev_open(struct inode*, struct file*);
static int dev_release(struct inode*, struct file*);
static ssize_t dev_read(struct file*, char*, size_t, loff_t*);
static ssize_t dev_write(struct file*, const char*, size_t, loff_t*);

static struct file_operations fops = {
   .open = dev_open,
   .read = dev_read,
   .write = dev_write,
   .release = dev_release,
};

static int major;

static int __init initModule(void) {
    major = register_chrdev(0, DEVICE_NAME, &fops);

    if (major < 0) {
        printk(KERN_ALERT "Random generator load failed\n");
        return major;
    }

    printk(KERN_INFO "Random generator module has been loaded: %d\n", major);
    return 0;
}

static void __exit exitModule(void) {
    unregister_chrdev(major, DEVICE_NAME);
    printk(KERN_INFO "Random generator module has been unloaded\n");
}

static int dev_open(struct inode *inodep, struct file *filep) {
   printk(KERN_INFO "Random generator device opened\n");
   return 0;
}

static ssize_t dev_write(struct file *filep, const char *buffer,
                         size_t len, loff_t *offset) {

   printk(KERN_INFO "Sorry, random generator is read only\n");
   return -EFAULT;
}

static int dev_release(struct inode *inodep, struct file *filep) {
   printk(KERN_INFO "Random generator device closed\n");
   return 0;
}

static ssize_t dev_read(struct file *filep, char *buffer, size_t len, loff_t *offset) {
    printk(KERN_INFO "Read\n");
    int i;
    get_random_bytes(&i,sizeof(i));
    char results[50];
    sprintf(results,"%d",i);
    if (*offset > 0){
    	return 0;
    }
    if (copy_to_user(buffer, results, 50))
    	return -EFAULT;
    *offset = *offset + 1;
    return strlen(results);
}

module_init(initModule);
module_exit(exitModule);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Khanh");
MODULE_DESCRIPTION("Random generator");
MODULE_SUPPORTED_DEVICE(DEVICE_NAME);